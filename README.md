### Connecting the board to Edge Impulse 
To start, we need to connect the board to our computer and to the website of edge-impulse in order to put a neural network on the board. The first step is to branch your STM32 discovery kit to your computer with a USB cable. To make sure that your STM32 is correctly connected, you need to verify that you can open the directory of the board from your computer. Once the board is connected to your computer, you can start to install the softwares needed to set up the board to receive a neural network from edge-impulse. 

The first software is node.js, where a version above 12 is required. For that we need to type 2 commands on our shell. One to get the resources from the node.js website and the other to install what we have just downloaded. If you have another version of node.js installed on your computer, you may have issues later in the process. Look in the annex for advice to resolve your issues. 

*curl -fsSL https://deb.nodesource.com/setup_12.x | sudo -E bash -*
*sudo apt-get install -y nodejs*


The second software is Edge-impulse CLI, which is a command line interface to have access to commands to control your board from your computer. It also works as a proxy between the board and the computer to have easier access to the data collected by the STM board. You will require Python3 or higher to make this software work. The install of the edge-impulse CLI is managed by node.js package manager : npm. Run the following command in your terminal. Cf 10
*npm install -g edge-impulse-cli *

	
Then we need to update the firmware of the board to fit with edge-impulse. You can download it from their website : https://cdn.edgeimpulse.com/firmware/DISCO-L475VG-IOT01A.bin
Then you just have to drag it from your file system to the board directory. Then the board will update its firmware itself. The update is finished when the LED stops flashing between red and green to stay green. 
The last step is to create a wizard that will set up the connection between the board and the website. It will ask you to connect with the same account you use for edge impulse website. If you have multiple projects, you will have to choose on which one you want to connect. It will also ask you if you want to set up a wifi connection from the board, since it’s not mandatory to use wifi, an annex can be found if you want to use it. The command to create a wizard is the following. You may need to use sudo before the commands depending on which rights you have on your computer. 

*edge-impulse-daemon*

	
If the connection works well , you should be able to see the board on the section “Devices” on the edge-impulse website. You are now ready to create your impulse neural network from the website. 

### Creation and training of an impulse neural network

In this part, our goal is to create and train a simple neural network and to make it work. However it allows us to have a preview of what a more complex neural network will require to work on a STM32 board. The following part will show you how to create a small neural network from edge impulse. 

As with many neural networks, the first step is to collect data and label it. On the website, go on the Data acquisition page from the studio website  https://studio.edgeimpulse.com/studio/20850 cf 11. There you will be able to collect sound from the board. You have to choose the frequency and the length of your audio signal. Choose your label and you start sampling as much as you want. We recommend having multiple people sampling to have better results after. You can modify/delete/add samples whenever you want.

You also have the ability to sample from your phone or your computer. In our case, we decided to sample from the phone since it was easier to collect data from many different people. However, results from the neural network on the board will decrease a bit since phone’s samples will be different from board’s samples.For our project, we decided to have 2 labels “oui” and “non” because we wanted a simple Neural network to test.

Following that, you will choose your parameters to define your impulse neural network in the “impulse design” panel on the website. The first one is the size of your data, make sure to have the same as your sample. Then choose the processing block to extract the useful data from your sample. We decided to choose the MFCC one which is designed to recognize human voice from audio signals. Then we choose our learning block which will classify the extract data. We use keras neural networks which have great efficiency with audio. Then save your impulse. These decisions were made in order to have a basic and small neural network, feel free to choose different parameters in order to have the neural network you want. 

On the MFCC panel, you can change parameters of the MFCC processing block, but what interests us the most is the “on-device performance” category. This category will tell you, for the board we have connected, the expected peak Ram usage and the processing time of the processing block. These statistics are crucial because they will tell you if your neural network can work on your board. Once you have parameters you want , you can save them. 

Then you can generate features from your dataset to create the training and test data for your neural network. In the NN classifier panel, you have some parameters that you can change before you start the training of your model. You can add or modify layers as much as you want. You will have a preview of the accuracy of your model, if you are not happy with your results, change the parameters to find a neural network that satisfies you. 

Following the training of your model, you can test it on your computer before uploading it on the board.
Our neural network is now ready to be built on the board. 

### Building the neural network on the board

First you need to choose, on the deployment panel on the website, on which board you want to deploy your neural network. We chose ST IoT Discovery Kit because our board is part of this family of boards. The website will now find optimizations of this neural network giving us the latency of the neural network, the ROM and RAM usage and the accuracy of the neural network. 

We download it, then dragged our binary file in our board and waited for the LED to stop blinking. Finally we ran the command to start the neural network from the board. As before, you may need to use sudo with this command. 
*edge-impulse-run-impulse*

The neural network will start and will display its parameters as a reminder. 

Then the board will collect 2s of sound nearly at every moment(There is some latency when giving results). The neural network will show the result in percentage between different labels. 

The result can sometimes be wrong because the neural network isn’t perfect but also because the microphone of the board is very sensitive. Therefore, if there is ambient sound, the neural network will be less accurate. 
